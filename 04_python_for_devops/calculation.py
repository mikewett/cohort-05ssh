#!/usr/bin/env python3
import calculatemod
import sys

# print(sys.argv)
try:
    print(
        "Result is: ",
        calculatemod.calculate(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3]),
    )
except ValueError:
    print("Values must be integers and an operator")
except IndexError:
    print("Must provide 2 numbers and an operator")