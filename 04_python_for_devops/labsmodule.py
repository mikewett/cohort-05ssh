#!/usr/bin/env python3

#basic calculate function
def digitsum(number):
    while number > 9:
        digittot=0
        chars=str(number)
        for char in chars:
            digittot+=(int(char))
        number=digittot
    return digittot


#function to calculate collatz series for a given number
def collatz(number):
    if number > 0:
        while number != 1:
            if number %2==0:
                number = number // 2
            else:
                number = number * 3 + 1
            print(number)
