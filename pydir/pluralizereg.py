#!/usr/bin/env python3
import re


def pluralize(word):
    if re.search("s$|x$|z$", word):
        print(word + "es")
    elif re.search("(?<![g,d,k,p,r,t])h", word):
        print(word + "es")
    elif re.search("(?<![a,e,i,o,u])y", word):
        temp = word[0 : (len(word) - 1)]
        print(temp + "ies")


pluralize("fly")
