#!/usr/bin/env python3

# import math module for log function
import math

# import sys module to process sys.args from command line
import sys


def calculate(op1, op2, operator):
    """[summary]

    Args:
        op1 ([type]): [description]
        op2 ([type]): [description]
        operator ([type]): [description]

    Returns:
        [type]: [description]
    """
    if operator == "+":
        return op1 + op2
    elif operator == "-":
        return op1 - op2
    elif operator == "*":
        return op1 * op2
    elif operator == "/":
        try:
            return op1 / op2
        except ZeroDivisionError:
            print("You cannot divide by zero")
            return 0
    elif operator == "log":
        try:
            return math.log(op1, op2)
        except ValueError:
            print("Invalid Log, no zero or negative numbers")
            return 0


# print(sys.argv)
try:
    print("Result is: ", calculate(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3]))
except ValueError:
    print("Values must be integers and an operator")
except IndexError:
    print("Must provide 2 numbers and an operator")
